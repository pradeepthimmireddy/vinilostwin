<?php
/**
 * TemplateMela
 * @copyright  Copyright (c) TemplateMela. (http://www.templatemela.com)
 * @license    http://www.templatemela.com/license/
 * @author         TemplateMela
 * @version        Release: 1.0
 */
/**  Set Default options : Theme Settings  */
function tm_set_default_options_child(){ 
	
	/*  General Settings  */	
	add_option("tmoption_bodyfont_color","787878"); // body font color
	add_option("tmoption_revslider_alias",'tm_homeslider_furniture');
	
	/*  Top Bar Settings  */
	add_option("tmoption_show_topbar","no"); // show top bar	
	add_option("tmoption_topbar_text_color","878787"); // topbar_text_color
	add_option("tmoption_topbar_link_color","B8B8B8"); // topbar_link_color
	add_option("tmoption_topbar_link_hover_color","FFFFFF"); // topbar_link_hover_color
	add_option("tmoption_top_menu_texthover_color","878787"); // Top menu text hover color
	
	add_option("tmoption_show_topbar_contact","no"); // show contact information
	
	/*  Header Settings  */	
	add_option("tmoption_header_background_upload", ''); // set header background
	add_option("tmoption_header_bkg_color","161B1E"); // header background color
	
	/*  Content Settings  */
	add_option("tmoption_link_color","878787"); // link color
	add_option("tmoption_hoverlink_color","161B1E"); // link hover color
	
	/*  Footer Settings  */	
	add_option("tmoption_footerbkg_color","161B1E"); // footer background color
	add_option("tmoption_footerlink_color","CCCCCC"); // footer link text color
	add_option("tmoption_footerhoverlink_color","ffffff"); // footer link hover text color	
}
add_action('init', 'tm_set_default_options_child'); 
function templatemela_load_fonts_child() {
    $fonts_url = '';
 
    /* Translators: If there are characters in your language that are not
    * supported by Lora, translate this to 'off'. Do not translate
    * into your own language.
    */
    $montserrat = _x( 'on', 'Montserrat font: on or off', 'harvest' );
 
    if ( 'off' !== $montserrat ) {
        $font_families = array();
 
        if ( 'off' !== $montserrat ) {
            $font_families[] = 'Montserrat:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic';			
        }
		
        $query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext' ),
        );
 
        $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
    } 
    return esc_url_raw( $fonts_url );
}
function google_fonts1() {
    wp_enqueue_style( 'google_fonts1', templatemela_load_fonts_child(), array(), '1.0.0' );
}
add_action( 'get_header', 'google_fonts1' );
function harvest_load_scripts_child() {		
	wp_enqueue_script( 'harvest_jquery_custom', get_stylesheet_directory_uri() . '/js/megnor/custom.js', array(), '', true);	  				 	
 }
add_action( 'wp_enqueue_scripts', 'harvest_load_scripts_child' );
?>